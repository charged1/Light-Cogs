import discord

from redbot.core import commands

class TechInfo(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['distro'])
    async def linuxdistro(self, ctx):
        """Sends an embed with links to download different linux distros."""
        embed = discord.Embed(title='Linux Distros', description='Select a link to download a linux distro!', color=0xe74c3c)
        embed.add_field(name='Ubuntu', value='https://www.ubuntu.com/download')
        embed.add_field(name='Mint', value='https://www.linuxmint.com/download.php')
        embed.add_field(name='Zorin', value='http://zorin-os.com/free.html')
        embed.add_field(name='Manjaro', value='https://manjaro.org/download/')
        embed.set_footer(text='More coming soon!')
        await ctx.send(embed=embed)
        
    @commands.command(aliases=['linuxusb'])
    async def linuxinstall(self, ctx):
        """Sends a tutorial to help you install linux."""
        embed = discord.Embed(title="How to install Linux (Most Distros)",
        description = "**You will need:** \n A working computer, with an OS installed and a USB drive (8GB) \n \n 1. Select the Linux Distro you want to use (`!distros` for a list of download links). \n \n 2. Use `!balenaetcher` to install Etcher, our flashing tool we'll use. \n \n 3. Open Etcher, and select your downloaded ISO file, your USB will already have been detected. Click Flash! \n \n 4. Once Flashing complete, restart your PC and spam your bootkey. Then Select your USB. \n \n 5. Boot into your Distro, and open the Installer \n \n 6. Go through the Installtion, then Enjoy!",
        color = 0xe74c3c)
        embed.set_footer(text="Let us know if we can impove on commands like this!")
        await ctx.reply(embed=embed, mention_author=False)
        
    @commands.command(aliases=['windowsusb'])
    async def windowsinstall(self, ctx):
        """Sends a tutorial to help you install Windows."""
        embed = discord.Embed(title="How to install Windows 10 from a Bootable USB",
        description = "**You will need:** \n A working computer, with an OS installed and a USB drive (8GB) \n \n 1. Download the Windows Media Creation tool, and run it: \n https://www.microsoft.com/en-gb/software-download/windows10 \n \n 2. Select Create Installation Media, when prompted. \n  \n 3. Select Edition, Language and others. \n \n 4. Select your USB. \n \n 5. Wait for the Flashing proccess to finish. \n \n 6. Restart the Computer, and spam your bootkey. \n \n 7. Go through the setup, enjoy!",
        color = 0xe74c3c)
        embed.set_footer(text="Let us know if we can impove on commands like this!")
        await ctx.reply(embed=embed, mention_author=False)
        
    @commands.command(aliases=['windows10', 'win10'])
    async def windows(self, ctx):
        """Sends a link to the Windows 10 download page."""
        embed = discord.Embed(title="Windows 10 Installer Download Link:",
        description = "https://www.microsoft.com/en-gb/software-download/windows10",
        color = 0xe74c3c)
        embed.set_footer(text="Use `!windowsinstall` for instructions.")
        await ctx.reply(embed=embed, mention_author=False)
        
    @commands.command(aliases=['etcher'])
    async def balenaetcher(self, ctx):
        """Sends a link to download Balena Etcher."""
        embed = discord.Embed(title="Balena Etcher Download Link",
        description = "https://balena.io/etcher",
        color = 0xe74c3c)
        embed.set_footer(text="Enjoy!")
        await ctx.reply(embed=embed, mention_author=False)

    @commands.command(aliases=['bytes'])
    async def malwarebytes(self, ctx):
        """Sends a link to download MalwareBytes antivirus."""
        embed = discord.Embed(title="Malware Bytes Download Link",
        description = "Keep your PC free from Viruses with Malwarebytes! \n \n **Download Link:** \n https://www.malwarebytes.com/",
        color = 0xe74c3c)
        embed.set_footer(text="Stay safe online!")
        await ctx.reply(embed=embed, mention_author=False)

    @commands.command(aliases=['windows7'])
    async def unsupportedos(self, ctx):
        """Learn why you shouldn't use Windows 7 and other unsupported operating systems."""
        embed = discord.Embed(
        title='Notice',
        description='We do not give support to those with unsupported operating systems such as Windows 8 and below. \nThis is because these Operating Systems no longer recieve security updates and can be dangerous to use.',
        color=0xe74c3c
        )
        embed.set_footer(text='Note: We do offer help in upgrading to a new OS from and unsupported one!')
        await ctx.send(embed=embed)

    @commands.command(aliases=['ts', 'tron'])
    async def tronscript(self, ctx):
        embed = discord.Embed(
        title='Tronscript Link',
        description='https://www.reddit.com/r/TronScript/',
        color=0xe74c3c
        )
        embed.set_footer(text='Suggested by Ethan Alexander.')
        await ctx.send(embed=embed)
        
def setup(bot):
    bot.add_cog(TechInfo(bot))
