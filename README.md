# Light-Cogs
Charged's cogs for Red! <br />
Read below for more information.

# Features and Cogs
**TechInfo** <br />
Lots of commands helping you and your devices!

**ExtendedAdmin** <br />
Extra admin commands, such as say, announce and more!

**Beta** <br />
BETA COMMANDS, very buggy.

# Installation
**Key:** <br />
`[p]` is your prefix. <br />
`[cog]` is the cog of your choice.

First, make sure you have the downloader installed. <br />
`[p]cog install downloader`

Then, we add the repository to the bot. <br />
`[p]repo add https://github.com/Charged5770/Light-Cogs/`

After that is done, we can install the cog/s to our bot. <br />
`[p]cog install Light-Cogs [cog]`

Finally, we load the cog. <br />
`[p]load [cog]`

**Updating the Cog**
`[p]cog update`

# Contact
If you still need help, DM me! Charged#5770 on discord.

# Credits
Thank You to BenTheTechGuy's TSC for some commands!
https://github.com/TechSupportCentral/TSCBot-py
